<?php
/**
 * This is the template for generating an action view file.
 */

/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $generator yii\gii\generators\controller\Generator */
/* @var $action string the action ID */

echo "<?php\n";
?>
/* @var $this fafcms\fafcms\components\ViewComponent */
<?= "?>" ?>

<h1><?= $generator->getControllerSubPath() . $generator->getControllerID() . '/' . $action ?></h1>

<p>
    You may change the content of this page by modifying
    the file <code><?= '<?=' ?> __FILE__; ?></code>.
</p>
