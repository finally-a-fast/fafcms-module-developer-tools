<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $generator fafcms\dev\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $properties array list of properties (property => [type, name. comment]) */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */
/* @var $generatedRelations string[] */
/* @var $modelNameSpace string */
/* @var $usedClasses string */
/* @var $activeRecordClass string */
/* @var $primaryColumns string|null */

use fafcms\fafcms\helpers\StringHelper;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Tab;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\Card;

$columns = [];

foreach ($tableSchema->columns as $column) {
    if (!in_array($column->name, [
        'id',
        'created_by',
        'updated_by',
        'activated_by',
        'deactivated_by',
        'deleted_by',
        'created_at',
        'updated_at',
        'activated_at',
        'deactivated_at',
        'deleted_at',
    ])) {
        $columns['field-' . $column->name] = [
            'class' => StringHelper::codeExportExpression($generator->useClass(FormField::class, $tableName) . '::class'),
            'settings' => [
                'field' => $column->name,
            ],
        ];
    }
}

$defaultView = [
    'default' => [
        'tab-1' => [
            'class' => StringHelper::codeExportExpression($generator->useClass(Tab::class, $tableName) . '::class'),
            'settings' => [
                'label' => ['fafcms-core', 'Master data'],
            ],
            'contents' => [
                'row-1' => [
                    'class' => StringHelper::codeExportExpression($generator->useClass(Row::class, $tableName) . '::class'),
                    'contents' => [
                        'column-1' => [
                            'class' => StringHelper::codeExportExpression($generator->useClass(Column::class, $tableName) . '::class'),
                            'settings' => [
                                'm' => 8
                            ],
                            'contents' => [
                                'card-1' => [
                                    'class' => StringHelper::codeExportExpression($generator->useClass(Card::class, $tableName) . '::class'),
                                    'settings' => [
                                        'title' => ['fafcms-core', 'Master data'],
                                        'icon' => 'playlist-edit',
                                    ],
                                    'contents' => $columns,
                                ],
                            ]
                        ],
                    ]
                ],
            ],
        ],
    ],
];
?>
    //region EditViewInterface implementation
    public static function editView(): array
    {
        return <?= StringHelper::codeExport($defaultView, 2) ?>;
    }
    //endregion EditViewInterface implementation
