<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $generator fafcms\dev\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $properties array list of properties (property => [type, name. comment]) */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */
/* @var $generatedRelations string[] */
/* @var $modelNameSpace string */
/* @var $usedClasses string */
/* @var $activeRecordClass string */
/* @var $primaryColumns string[]|null */

use yii\db\Schema;
use yii\helpers\ArrayHelper;
use fafcms\fafcms\helpers\StringHelper;
use fafcms\fafcms\items\DataColumn;
use fafcms\fafcms\items\ActionColumn;

$class = StringHelper::codeExportExpression($generator->useClass(DataColumn::class, $tableName) . '::class');
$columns = [];

$columnNames = ArrayHelper::getColumn($tableSchema->columns, 'name');

$hasId = isset($columnNames['id']);
$hasStatus = isset($columnNames['status']);
$hasName = ($primaryColumns !== null && (count($primaryColumns) > 1 || $primaryColumns[0] !== 'id'));

$sortCount = ($hasId ? 1 : 0) + ($hasStatus ? 1 : 0) + ($hasName ? count($primaryColumns) : 0);

foreach ($tableSchema->columns as $tableColumn) {
    if (in_array($tableColumn->name, [
        'created_by',
        'updated_by',
        'activated_by',
        'deactivated_by',
        'deleted_by',
        'created_at',
        'updated_at',
        'activated_at',
        'deactivated_at',
        'deleted_at',
    ])) {
        continue;
    }

    $relation = $generator->tableColumnRelations[$tableName][$tableColumn->name] ?? null;

    $primaryColumnIndex = array_search($tableColumn->name, $primaryColumns, true);

    $link = (
        $primaryColumnIndex !== false ||
        (
            in_array($tableColumn->type, [Schema::TYPE_STRING, Schema::TYPE_TEXT, Schema::TYPE_INTEGER], true) &&
            StringHelper::contains($tableColumn->name, [
                'media',
                'file',
            ])
        ) ||
        $relation !== null
    );

    if ($tableColumn->name === 'id') {
        $sort = 1;
    } elseif ($tableColumn->name === 'status') {
        $sort = 2;
    } elseif ($primaryColumnIndex !== false) {
        $sort = 3 + $primaryColumnIndex;
    } else {
        $sortCount++;
        $sort = $sortCount;
    }

    $column = [
        'class' => $class,
        'settings' => [
            'field' => $tableColumn->name,
            'sort' => $sort,
        ],
    ];

    if ($link) {
        $column['settings']['link'] = true;
    }

    $columns[$tableColumn->name] = $column;
}

ArrayHelper::multisort($columns, 'settings.sort');

$columns['action-column'] = [
    'class' => StringHelper::codeExportExpression($generator->useClass(ActionColumn::class, $tableName) . '::class')
];
?>
    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => <?= StringHelper::codeExport($columns, 3) . PHP_EOL ?>
        ];
    }
    //endregion IndexViewInterface implementation
