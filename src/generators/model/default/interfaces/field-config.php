<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $generator fafcms\dev\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $properties array list of properties (property => [type, name. comment]) */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */
/* @var $generatedRelations string[] */
/* @var $modelNameSpace string */
/* @var $usedClasses string */
/* @var $activeRecordClass string */
/* @var $primaryColumns string|null */

use fafcms\fafcms\inputs\{
    DatePicker,
    DropDownList,
    EmailInput,
    PhoneInput,
    ExtendedDropDownList,
    TextInput,
    Textarea,
    TimePicker,
    DateTimePicker,
    SwitchCheckbox,
    NumberInput,
    UrlInput,
};
use fafcms\filemanager\inputs\FileSelect;
use yii\db\Schema;
use fafcms\fafcms\helpers\StringHelper;

$columns = [];

foreach ($tableSchema->columns as $column) {
    $relation = $generator->tableColumnRelations[$tableName][$column->name] ?? null;

    if ($column->name === 'status') {
        $columns[$column->name] = [
            'type'     => StringHelper::codeExportExpression($generator->useClass(DropDownList::class, $tableName) . '::class'),
            'items'   => StringHelper::codeExportExpression('$this->getAttributeOptions(\'' . $column->name .'\', false)'),
        ];
    } elseif (in_array($column->type, [Schema::TYPE_STRING, Schema::TYPE_TEXT], true) &&
        StringHelper::contains($column->name, [
            'email',
        ])
    ) {
        $columns[$column->name] = [
            'type' => StringHelper::codeExportExpression($generator->useClass(EmailInput::class, $tableName) . '::class'),
        ];
    } elseif (in_array($column->type, [Schema::TYPE_STRING, Schema::TYPE_TEXT], true) &&
        StringHelper::contains($column->name, [
            'phone', 'telephone', 'tel', 'mobile', 'fax'
        ])
    ) {
        $columns[$column->name] = [
            'type' => StringHelper::codeExportExpression($generator->useClass(PhoneInput::class, $tableName) . '::class'),
        ];
    } elseif (in_array($column->type, [Schema::TYPE_STRING, Schema::TYPE_TEXT], true) &&
        StringHelper::contains($column->name, [
            'url', 'website'
        ])
    ) {
        $columns[$column->name] = [
            'type' => StringHelper::codeExportExpression($generator->useClass(UrlInput::class, $tableName) . '::class'),
        ];
    } elseif (in_array($column->type, [Schema::TYPE_STRING, Schema::TYPE_TEXT, Schema::TYPE_INTEGER], true) &&
        StringHelper::contains($column->name, [
            'media',
            'file',
            'image',
            'picture',
            'video',
            'movie',
            'archive',
            'document',
        ]) &&
        !StringHelper::contains($column->name, [
            'filegroup',
        ])
    ) {
        $columns[$column->name] = [
            'type' => StringHelper::codeExportExpression($generator->useClass(FileSelect::class, $tableName) . '::class'),
        ];

        if (StringHelper::contains($column->name, [
            'image',
            'picture',
        ])) {
            $columns[$column->name]['mediatypes'] = ['image'];
        }

        if (StringHelper::contains($column->name, [
            'video',
            'movie',
        ])) {
            $columns[$column->name]['mediatypes'] = ['video'];
        }

        if (StringHelper::contains($column->name, ['archive'])) {
            $columns[$column->name]['mediatypes'] = ['archive'];
        }

        if (StringHelper::contains($column->name, ['document'])) {
            $columns[$column->name]['mediatypes'] = ['document'];
        }

        if ($column->type === Schema::TYPE_INTEGER) {
            $columns[$column->name]['external'] = false;
        }
    } elseif ($relation !== null) {
        $columns[$column->name] = [
            'type'     => StringHelper::codeExportExpression($generator->useClass(ExtendedDropDownList::class, $tableName) . '::class'),
            'items'   => StringHelper::codeExportExpression('$this->getAttributeOptions(\'' . $column->name .'\', false)'),
            'relationClassName' => StringHelper::codeExportExpression($generator->useClass($relation['tableMeta']['fullyQualifiedClassName'], $tableName) . '::class'),
        ];
    } elseif ($column->type === Schema::TYPE_BOOLEAN || ($column->type === Schema::TYPE_TINYINT && $column->size === 1)) {
        $columns[$column->name] = [
            'type'     => StringHelper::codeExportExpression($generator->useClass(SwitchCheckbox::class, $tableName) . '::class'),
            'offLabel' => ['fafcms-core', 'No'],
            'onLabel'  => ['fafcms-core', 'Yes']
        ];
    } elseif ($column->type === Schema::TYPE_TIME) {
        $columns[$column->name] = [
            'type' => StringHelper::codeExportExpression($generator->useClass(TimePicker::class, $tableName) . '::class'),
        ];
    } elseif ($column->type === Schema::TYPE_DATE) {
        $columns[$column->name] = [
            'type' => StringHelper::codeExportExpression($generator->useClass(DatePicker::class, $tableName) . '::class'),
        ];
    } elseif ($column->type === Schema::TYPE_DATETIME || $column->type === Schema::TYPE_TIMESTAMP) {
        $columns[$column->name] = [
            'type' => StringHelper::codeExportExpression($generator->useClass(DateTimePicker::class, $tableName) . '::class'),
        ];
    } elseif ($column->type === Schema::TYPE_STRING) {
        $columns[$column->name] = [
            'type' => StringHelper::codeExportExpression($generator->useClass(TextInput::class, $tableName) . '::class'),
        ];
    } elseif ($column->type === Schema::TYPE_TEXT || $column->type === Schema::TYPE_JSON) {
        $columns[$column->name] = [
            'type' => StringHelper::codeExportExpression($generator->useClass(Textarea::class, $tableName) . '::class'),
        ];
    } elseif (in_array($column->type, [
            Schema::TYPE_SMALLINT,
            Schema::TYPE_INTEGER,
            Schema::TYPE_BIGINT,
            Schema::TYPE_TINYINT,
            Schema::TYPE_FLOAT,
            Schema::TYPE_DOUBLE,
            Schema::TYPE_DECIMAL,
            Schema::TYPE_MONEY
        ], true)) {
        $columns[$column->name] = [
            'type' =>  StringHelper::codeExportExpression($generator->useClass(NumberInput::class, $tableName) . '::class'),
        ];
    } else {
        $columns[$column->name] = [
            'type' => $column->type
        ];
    }

    if (in_array($column->name, [
        'id',
        'created_by',
        'updated_by',
        'activated_by',
        'deactivated_by',
        'deleted_by',
        'created_at',
        'updated_at',
        'activated_at',
        'deactivated_at',
        'deleted_at',
    ])) {
        $columns[$column->name]['options']['disabled'] = true;
    }

    if (!empty($column->comment)) {
        $columns[$column->name]['description'] = $column->comment;
    }
}
?>
    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return <?= StringHelper::codeExport($columns, 2) ?>;
    }
    //endregion FieldConfigInterface implementation
