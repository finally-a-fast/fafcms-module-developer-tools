<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $generator fafcms\dev\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $properties array list of properties (property => [type, name. comment]) */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */
/* @var $generatedRelations string[] */
/* @var $modelNameSpace string */
/* @var $usedClasses string */
/* @var $activeRecordClass string */
/* @var $primaryColumns string|null */

use fafcms\fafcms\helpers\StringHelper;

$columns = [];

foreach ($tableSchema->columns as $column) {
    $relation = $generator->tableColumnRelations[$tableName][$column->name] ?? null;

    if ($column->name === 'status') {
        $columns[$column->name] = [
            StringHelper::codeExportExpression('static::STATUS_ACTIVE') => StringHelper::codeExportExpression('Yii::t(\'fafcms-core\', \'Active\')'),
            StringHelper::codeExportExpression('static::STATUS_INACTIVE') => StringHelper::codeExportExpression('Yii::t(\'fafcms-core\', \'Inactive\')')
        ];
    } elseif ($relation !== null) {
        $optionClass = $generator->useClass($relation['tableMeta']['fullyQualifiedClassName'], $tableName);

        if ($optionClass === 'static' || !method_exists($relation['tableMeta']['fullyQualifiedClassName'], 'getOptions')) {
            $columns[$column->name] = StringHelper::codeExportExpression('static function($properties = []) {' . PHP_EOL .
                '        return ' . $optionClass . '::getOptionProvider($properties)->getOptions();' . PHP_EOL .
            '    }');
        } else {
            $columns[$column->name] = StringHelper::codeExportExpression('static function(...$params) {' . PHP_EOL .
                '        return ' . $optionClass . '::getOptions(...$params);' . PHP_EOL .
            '    }');
        }
    }
}
?>
    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return <?= StringHelper::codeExport($columns, 2) ?>;
    }
    //endregion AttributeOptionTrait implementation
