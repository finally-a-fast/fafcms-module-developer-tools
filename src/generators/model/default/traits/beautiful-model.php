<?php
/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $generator fafcms\dev\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $queryClassName string query class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $properties array list of properties (property => [type, name. comment]) */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */

use yii\helpers\Inflector;
?>
    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return <?= (isset($tableMeta['bootstrap']) && $tableMeta['namespace'] !== 'fafcms\\fafcms\\models' ? $generator->useClass($tableMeta['bootstrap'], $tableName) . '::$id . \'/' : '\'') ?><?= strtolower($className) ?>';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  '<?= strtolower($className) ?>';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return <?= $generator->generateString(Inflector::pluralize($className)) ?>;
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return <?= $generator->generateString(Inflector::singularize($className)) ?>;
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['<?= implode('\'] ?? \'\') . \' \' . ($model[\'', $generator->getPrimaryColumns($tableSchema)) ?>'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation
