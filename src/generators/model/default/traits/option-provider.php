<?php
/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $generator fafcms\dev\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $queryClassName string query class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $properties array list of properties (property => [type, name. comment]) */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */
?>
    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): <?= $generator->useClass(\fafcms\helpers\classes\OptionProvider::class, $tableName) . PHP_EOL ?>
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.<?= implode('\', static::tableName() . \'.', $generator->getPrimaryColumns($tableSchema)) ?>'
            ])
            ->setSort([static::tableName() . '.<?= implode('\' => SORT_ASC, static::tableName() . \'.', $generator->getPrimaryColumns($tableSchema)) ?>' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation
