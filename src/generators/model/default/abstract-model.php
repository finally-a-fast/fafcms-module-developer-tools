<?php
/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $generator fafcms\dev\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $abstractModelClassName string class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $properties array list of properties (property => [type, name. comment]) */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */
/* @var $generatedRelations string[] */
/* @var $modelNameSpace string */
/* @var $usedClasses string */
/* @var $activeRecordClass string */

use fafcms\fafcms\helpers\StringHelper;

echo '<?php' . PHP_EOL;
?>

namespace <?= $modelNameSpace ?>;

<?= $usedClasses ?>

/**
 * This is the abstract model class for table "<?= $generator->generateTableName($tableName) ?>".
 *
 * @package <?= $modelNameSpace . PHP_EOL ?>
 *
 * @property-read array $fieldConfig
 *
<?php foreach ($properties as $property => $data): ?>
 * @property <?= "{$data['type']} \${$property}"  . ($data['comment'] ? ' ' . strtr($data['comment'], ["\n" => ' ']) : '') . PHP_EOL ?>
<?php endforeach; ?>
<?php if (!empty($relations)): ?>
 *
<?php foreach ($relations as $name => $relation): ?>
 * @property <?= $relation['tableMeta']['class'] . ($relation['type'] === 'hasMany' ? '[]' : '') . ' $' . lcfirst($name) . "\n" ?>
<?php endforeach; ?>
<?php endif; ?>
 */
abstract class <?= $abstractModelClassName ?> extends <?= $activeRecordClass . (!empty($interfaces) ? ' implements ' . implode(', ', array_keys($interfaces)) : '') . "\n" ?>
{
<?php if (!empty($traits)): ?>
<?php foreach ($traits as $traitName => $trait): ?>
    use <?= $traitName ?>;
<?php endforeach; ?>

<?php endif; ?>
<?php foreach ($traits as $traitName => $trait): ?>
<?php if (!empty($trait)): ?>
<?= $trait ?>

<?php endif; ?>
<?php endforeach; ?>
<?php if (!empty($interfaces)): ?>
<?php foreach ($interfaces as $interfaceName => $interface): ?>
<?php if (!empty($interface)): ?>
<?= $interface ?>

<?php endif; ?>
<?php endforeach; ?>
<?php endif; ?>
    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '<?= $generator->generateTableName($tableName) ?>';
    }
<?php if ($generator->db !== 'db'): ?>

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('<?= $generator->db ?>');
    }
<?php endif; ?>

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [<?= empty($rules) ? '' : ("\n            " . implode(",\n            ", $rules) . ",\n        ") ?>]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
<?php foreach ($labels as $name => $label): ?>
            <?= "'$name' => " . $generator->generateString($label) . ",\n" ?>
<?php endforeach; ?>
        ]);
    }
<?php foreach ($generatedRelations as $generatedRelation): ?>
<?= PHP_EOL . StringHelper::indent($generatedRelation, 4) . PHP_EOL ?>
<?php endforeach; ?>
}
