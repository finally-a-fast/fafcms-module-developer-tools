<?php
/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $generator fafcms\dev\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $abstractModelClassName string class name */
/* @var $modelNameSpace string */
/* @var $abstractModelNameSpace string abstract namespace */

echo '<?php' . PHP_EOL;
?>

namespace <?= $modelNameSpace ?>;

use <?= $abstractModelNameSpace . '\\' . $abstractModelClassName . ';' . PHP_EOL ?>

/**
 * This is the model class for table "<?= $generator->generateTableName($tableName) ?>".
 *
 * @package <?= $modelNameSpace . PHP_EOL ?>
 */
class <?= $className ?> extends <?= $abstractModelClassName . PHP_EOL ?>
{

}
