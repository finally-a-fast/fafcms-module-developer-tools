<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace fafcms\dev\generators\model;

use fafcms\fafcms\helpers\StringHelper;
use fafcms\helpers\ActiveRecord;
use fafcms\helpers\interfaces\EditViewInterface;
use fafcms\helpers\interfaces\FieldConfigInterface;
use fafcms\helpers\interfaces\IndexViewInterface;
use fafcms\helpers\traits\AttributeOptionTrait;
use fafcms\helpers\traits\BeautifulModelTrait;
use fafcms\helpers\traits\OptionProviderTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\NotSupportedException;
use yii\db\ActiveQuery;

use yii\db\Connection;
use yii\db\Exception;
use yii\db\Schema;
use yii\db\TableSchema;
use yii\gii\CodeFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\validators\DateValidator;

/**
 * Class Generator
 *
 * @package fafcms\dev\generators\model
 */
class Generator extends \yii\gii\Generator
{
    const JUNCTION_RELATION_VIA_TABLE = 'table';
    const JUNCTION_RELATION_VIA_MODEL = 'model';

    public string $icon = 'database-edit-outline';
    public string $db = 'db';
    public string $ns = 'app\\models';
    public string $abstractNs = 'app\\abstracts\\models';
    public $tableName;
    public $modelClass;
    public $enableI18N = true;
    public $messageCategory = '';
    public string $baseClass = ActiveRecord::class;
    public string $generateJunctionRelationMode = self::JUNCTION_RELATION_VIA_TABLE;
    public bool $generateRelationsFromCurrentSchema = true;
    public bool $useSchemaName = true;
    public bool $useRealClasses = true;
    public string $ignoreColumns = 'translation_base_id, projectlanguage_id, project_id';
    public string $ignoreNamespaces = 'app\\';

    /** @var string  */
    protected string $generatedModelClass;

    public array $interfaces = [
        FieldConfigInterface::class  => 'interfaces/field-config',
        IndexViewInterface::class => 'interfaces/index-view',
        EditViewInterface::class => 'interfaces/edit-view',
    ];

    public array $traits = [
        BeautifulModelTrait::class => 'traits/beautiful-model',
        OptionProviderTrait::class => 'traits/option-provider',
        AttributeOptionTrait::class => 'traits/attribute-option',
    ];

    public array $possiblePrimaryColumns = [
        'name',
        ['firstname', 'lastname'],
        'firstname',
        'lastname',
        'title',
        'label',
        'headline',
        'id',
    ];

    /**
     * @var array[]
     */
    public array $usedClasses = [];

    public function getName()
    {
        return 'Model';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'This generator generates an ActiveRecord class for the specified database table.';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['db', 'ns', 'abstractNs', 'tableName', 'baseClass'], 'filter', 'filter' => 'trim'],
            [['ns', 'abstractNs'], 'filter', 'filter' => function ($value) { return trim($value, '\\'); }],
            [['db', 'ns', 'abstractNs', 'tableName', 'baseClass'], 'required'],
            [['db', 'modelClass'], 'match', 'pattern' => '/^\w+$/', 'message' => 'Only word characters are allowed.'],
            [['ns', 'abstractNs', 'baseClass'], 'match', 'pattern' => '/^[\w\\\\]+$/', 'message' => 'Only word characters and backslashes are allowed.'],
            [['tableName'], 'match', 'pattern' => '/^([\w ]+\.)?([\w\-\* ]+)$/', 'message' => 'Only word characters, and optionally spaces, an asterisk and/or a dot are allowed.'],
            [['db'], 'validateDb'],
            [['ns', 'abstractNs'], 'validateNamespace'],
            [['baseClass'], 'validateClass', 'params' => ['extends' => ActiveRecord::class]],
            [['generateJunctionRelationMode'], 'in', 'range' => [self::JUNCTION_RELATION_VIA_TABLE, self::JUNCTION_RELATION_VIA_MODEL]],
            [['useSchemaName', 'generateRelationsFromCurrentSchema', 'useRealClasses'], 'boolean'],
            [['ignoreColumns', 'ignoreNamespaces'], 'string']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'ns' => 'Namespace for tables without module prefix',
            'abstractNs' => 'Abstract namespace for tables without module prefix',
            'db' => 'Database Connection ID',
            'tableName' => 'Table Name',
            'baseClass' => 'Base Class',
            'generateJunctionRelationMode' => 'Generate Junction Relations As',
            'generateRelationsFromCurrentSchema' => 'Generate Relations from Current Schema',
            'useSchemaName' => 'Use Schema Name',
            'useRealClasses' => 'Use real classes and ignore the injector configuration',
            'ignoreColumns' => 'Ignore these columns at model generation',
            'ignoreNamespaces' => 'Ignore relations which are in the specified namespaces'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function hints()
    {
        return array_merge(parent::hints(), [
            'ns' => 'This is the namespace of the ActiveRecord class to be generated, e.g., <code>app\models</code> only used when table has no module prefix.',
            'abstractNs' => 'This is the namespace of the ActiveRecord class to be generated, e.g., <code>app\models</code> only used when table has no module prefix.',
            'db' => 'This is the ID of the DB application component.',
            'tableName' => 'This is the name of the DB table that the new ActiveRecord class is associated with, e.g. <code>post</code>.
                The table name may consist of the DB schema part if needed, e.g. <code>public.post</code>.
                The table name may end with asterisk to match multiple table names, e.g. <code>tbl_*</code>
                will match tables who name starts with <code>tbl_</code>. In this case, multiple ActiveRecord classes
                will be generated, one for each matching table name; and the class names will be generated from
                the matching characters. For example, table <code>tbl_post</code> will generate <code>Post</code>
                class.',
            'baseClass' => 'This is the base class of the new ActiveRecord class. It should be a fully qualified namespaced class name.',
            'generateJunctionRelationMode' => 'This indicates whether junction relations are generated with `viaTable()` or `via()` (Via Model) relations.
                Make sure you also generate the junction models when using the "Via Model" option.
            ',
            'generateRelationsFromCurrentSchema' => 'This indicates whether the generator should generate relations from current schema or from all available schemas.',
            'useSchemaName' => 'This indicates whether to include the schema name in the ActiveRecord class
                when it\'s auto generated. Only non default schema would be used.',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function requiredTemplates()
    {
        return ['model.php'];
    }

    /**
     * {@inheritdoc}
     */
    public function stickyAttributes()
    {
        return array_merge(parent::stickyAttributes(), ['ns', 'abstractNs', 'db', 'baseClass', 'generateJunctionRelationMode']);
    }

    /**
     * Returns the `tablePrefix` property of the DB connection as specified
     *
     * @return string
     * @since 2.0.5
     * @see getDbConnection
     */
    public function getTablePrefix()
    {
        $db = $this->getDbConnection();
        if ($db !== null) {
            return $db->tablePrefix;
        }

        return '';
    }

    /**
     * @var array
     */
    public array $tableColumnRelations = [];

    /**
     * @param TableSchema $table
     *
     * @return TableSchema
     */
    protected function removeCustomColumns(TableSchema $table): TableSchema
    {
        foreach ($table->columns as $index => $column) {
            if ($this->isIgnoredColumn($column->name)) {
                unset($table->columns[$index]);
            }
        }

        return $table;
    }

    /**
     * @param string $columnName
     *
     * @return bool
     */
    protected function isIgnoredColumn(string $columnName): bool
    {
        $ignoreColumns = array_map('trim', explode(',', $this->ignoreColumns));

        return preg_match('/^cf_[\d]*_(.*)$/i', $columnName) || in_array($columnName, $ignoreColumns);
    }

    /**
     * {@inheritdoc}
     * @return array
     * @throws Exception
     * @throws InvalidConfigException
     * @throws NotSupportedException
     * @throws \ReflectionException
     */
    public function generate(): array
    {
        $files = [];
        $db = $this->getDbConnection();

        $relations = [];
        $schemaNames = $this->getSchemaNames();

        foreach ($schemaNames as $schemaName) {
            foreach ($db->getSchema()->getTableSchemas($schemaName) as $table) {
                $table = $this->removeCustomColumns($table);
                $tableMeta = $this->getTableMeta($table->fullName);

                foreach ($table->foreignKeys as $foreignKey => $refs) {
                    $refTable = $refs[0];
                    $refTableSchema = $db->getTableSchema($refTable);
                    $refTableSchema = $this->removeCustomColumns($refTableSchema);

                    if ($refTableSchema === null) {
                        // Foreign key could point to non-existing table: https://github.com/yiisoft/yii2-gii/issues/34
                        continue;
                    }

                    unset($refs[0]);
                    $fks = array_keys($refs);

                    if ($this->isIgnoredColumn($fks[0])) {
                        continue;
                    }

                    $refTableMeta = $this->getTableMeta($refTable);
                    try {
                        $relationName = $this->generateRelationName($relations, $table, $fks[0], false, array_flip($refs));
                    } catch (InvalidConfigException $e) {
                        //var_dump($refTable); TODO
                        continue;
                    }

                    $relation = [
                        'type' => 'hasOne',
                        'tableMeta' => $refTableMeta,
                        'link' => array_flip($refs),
                        'relationName' => $relationName
                    ];

                    $relations[$table->fullName][$relationName] = $relation;
                    $this->tableColumnRelations[$table->fullName][$fks[0]] = $relation;

                    // Add relation for the referenced table
                    $hasMany = $this->isHasManyRelation($table, $fks);
                    $relationName = $this->generateRelationName($relations, $refTableSchema, $tableMeta['class'], $hasMany, $refs);


                    $ignoreNamespaces = trim($this->ignoreNamespaces);

                    if (!empty($ignoreNamespaces)) {
                        foreach (explode(',', $ignoreNamespaces) as $ignoreNamespace) {
                            if (mb_stripos($tableMeta['fullyQualifiedClassName'], trim($ignoreNamespace)) === 0) {
                                continue 2;
                            }
                        }
                    }

                    $relation = [
                        'type' => ($hasMany ? 'hasMany' : 'hasOne'),
                        'tableMeta' => $tableMeta,
                        'link' => $refs,
                        'relationName' => $relationName
                    ];

                    $relations[$refTableSchema->fullName][$relationName] = $relation;
                }
            }

            foreach ($db->getSchema()->getTableSchemas($schemaName) as $table) {
                $table = $this->removeCustomColumns($table);

                if (($junctionFks = $this->checkJunctionTable($table)) === false) {
                    continue;
                }

                //var_dump('TODO MANY MANY...');die();
                $relations = $this->generateManyManyRelations($table, $junctionFks, $relations);
            }
        }

        foreach ($relations as &$relation) {
            ksort($relation);
        }

        foreach ($this->getTableNames() as $tableName) {
            // model :
            $tableMeta = $this->getTableMeta($tableName);
            $modelClassName = $tableMeta['class'];
            $this->generatedModelClass =  $tableMeta['namespace'] . '\\' . $modelClassName;
            $abstractModelClassName = 'Base' . $tableMeta['class'];
            $this->messageCategory = $tableMeta['messageCategory'];
            $modelNameSpace = $tableMeta['abstract-namespace'];

            $tableRelations = $relations[$tableName] ?? [];
            $tableSchema = $db->getTableSchema($tableName);
            $tableSchema = $this->removeCustomColumns($tableSchema);

            $this->useClass(Yii::class, $tableName);

            $params = [
                'tableName' => $tableName,
                'className' => $modelClassName,
                'abstractModelClassName' => $abstractModelClassName,
                'tableSchema' => $tableSchema,
                'relations' => $tableRelations,
                'modelNameSpace' => $modelNameSpace,
                'primaryColumns' => $this->getPrimaryColumns($tableSchema),
                'activeRecordClass' => $this->useClass($this->baseClass, $tableName),
                'generatedRelations' => $this->generatedRelations($tableRelations, $tableName),
                'tableMeta' => $tableMeta
            ];

            $traits = [];

            foreach ($this->traits as $trait => $traitFile) {
                $this->usedClasses[$tableName][]  = $trait;
                $traits[$trait] = $this->render($traitFile . '.php', $params);
            }

            $interfaces = [];

            foreach ($this->interfaces as $interface => $interfaceFile) {
                $this->usedClasses[$tableName][]  = $interface;
                $interfaces[$interface] = $this->render($interfaceFile . '.php', $params);
            }

            $files[] = new CodeFile(
                Yii::getAlias('@' . str_replace('\\', '/', $modelNameSpace)) . '/' . $abstractModelClassName . '.php',
                $this->render('abstract-model.php', array_merge($params, [
                    'properties' => $this->generateProperties($tableSchema),
                    'labels' => $this->generateLabels($tableSchema),
                    'rules' => $this->generateRules($tableSchema, $tableName),
                    'usedClasses' => $this->getSortedUsedClasses($tableName),
                    'interfaces' => $this->getClassesWithoutNamespace($interfaces, true),
                    'traits' => $this->getClassesWithoutNamespace($traits, true),
                ]))
            );

            $files[] = new CodeFile(
                Yii::getAlias('@' . str_replace('\\', '/', $tableMeta['namespace'])) . '/' . $modelClassName . '.php',
                $this->render('model.php', [
                    'abstractModelNameSpace' => $modelNameSpace,
                    'modelNameSpace' => $tableMeta['namespace'],
                    'tableName' => $tableName,
                    'className' => $modelClassName,
                    'abstractModelClassName' => $abstractModelClassName,
                ])
            );
        }

        $db->getSchema()->refresh();

        return $files;
    }

    /**
     * @param array  $relations
     * @param string $tableName
     *
     * @return array
     */
    protected function generatedRelations(array $relations, string $tableName): array
    {
        $generatedRelations = [];

        foreach ($relations as $name => $relation) {
            if (($relation['type'] ?? null) !== null) {
                $relationCode = 'return $this->' . $relation['type'] . '(' . $this->useClass($relation['tableMeta']['fullyQualifiedClassName'], $tableName) . '::class, ' . StringHelper::codeExport($relation['link'], 1) . ');';
            } else {
                $relationCode = $relation[0] ?? '';
            }

            $activeQuery = $this->useClass(ActiveQuery::class, $tableName);

            $generatedRelations[] = <<<PHP
/**
 * Gets query for [[$name]].
 *
 * @return $activeQuery
 */
public function get$name(): $activeQuery
{
    $relationCode
}
PHP;
        }

        return $generatedRelations;
    }

    /**
     * @param TableSchema $table
     *
     * @return array
     */
    protected function generateProperties(TableSchema $table): array
    {
        $properties = [];

        foreach ($table->columns as $column) {
            switch ($column->type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_TINYINT:
                    $type = 'int';
                    break;
                case Schema::TYPE_BOOLEAN:
                    $type = 'bool';
                    break;
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                    $type = 'float';
                    break;
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                case Schema::TYPE_JSON:
                    $type = 'string';
                    break;
                default:
                    $type = $column->phpType;
            }

            if ($column->allowNull) {
                $type .= '|null';
            }

            $properties[$column->name] = [
                'type' => $type,
                'name' => $column->name,
                'comment' => $column->comment,
            ];
        }

        return $properties;
    }

    /**
     * @param TableSchema $table
     *
     * @return array
     */
    public function generateLabels(TableSchema $table): array
    {
        $labels = [];

        foreach ($table->columns as $column) {
            if (!strcasecmp($column->name, 'id')) {
                $labels[$column->name] = 'ID';
            } else {
                $label = Inflector::camel2words($column->name);

                if (!empty($label) && substr_compare($label, ' id', -3, 3, true) === 0) {
                    $label = substr($label, 0, -3) . ' ID';
                }

                $labels[$column->name] = $label;
            }
        }

        return $labels;
    }

    /**
     * @param TableSchema $table
     * @param string      $tableName
     *
     * @return array
     */
    public function generateRules(TableSchema $table, string $tableName): array
    {
        $types = [];
        $lengths = [];

        foreach ($table->columns as $column) {
            if ($this->isIgnoredColumn($column->name) || $column->autoIncrement) {
                continue;
            }

            if (!$column->allowNull && $column->defaultValue === null) {
                $types['required'][] = $column->name;
            }

            if ($column->type === Schema::TYPE_TINYINT && $column->size === 1) {
                $types['boolean'][] = $column->name;
            } else {
                switch ($column->type) {
                    case Schema::TYPE_SMALLINT:
                    case Schema::TYPE_INTEGER:
                    case Schema::TYPE_BIGINT:
                    case Schema::TYPE_TINYINT:
                        $types['integer'][] = $column->name;
                        break;
                    case Schema::TYPE_BOOLEAN:
                        $types['boolean'][] = $column->name;
                        break;
                    case Schema::TYPE_FLOAT:
                    case Schema::TYPE_DOUBLE:
                    case Schema::TYPE_DECIMAL:
                    case Schema::TYPE_MONEY:
                        $types['number'][] = $column->name;
                        break;
                    case Schema::TYPE_DATE:
                        $types['date'][] = $column->name;
                        break;
                    case Schema::TYPE_TIME:
                        $types['time'][] = $column->name;
                        break;
                    case Schema::TYPE_DATETIME:
                    case Schema::TYPE_TIMESTAMP:
                        $types['datetime'][] = $column->name;
                        break;
                    case Schema::TYPE_JSON:
                        $types['safe'][] = $column->name;
                        break;
                    default: // strings
                        if ($column->size > 0) {
                            $lengths[$column->size][] = $column->name;
                        } else {
                            $types['string'][] = $column->name;
                        }
                }
            }
        }

        $rules = [];
        $driverName = $this->getDbDriverName();

        foreach ($types as $type => $columns) {
            foreach ($columns as $column) {
                if ($driverName === 'pgsql' && $type === 'integer') {
                    $rules[] = '\'' . $type . '-' . $column . '\'' . ' => [\'' . $column . '\', \'default\', \'value\' => null]';
                }

                if ($type === 'datetime') {
                    $rules[] = '\'date-' . $column . '\'' . ' => [\'' . $column . '\', \'date\', \'type\' => ' . $this->useClass(DateValidator::class, $tableName) . '::TYPE_DATETIME, \'timestampAttribute\' => \'' . $column . '\', \'timestampAttributeFormat\' => \'php:Y-m-d H:i:s\']';
                } elseif ($type === 'date') {
                    $rules[] = '\'date-' . $column . '\'' . ' => [\'' . $column . '\', \'date\', \'type\' => ' . $this->useClass(DateValidator::class, $tableName) . '::TYPE_DATE, \'timestampAttribute\' => \'' . $column . '\', \'timestampAttributeFormat\' => \'php:Y-m-d\']';
                } elseif ($type === 'time') {
                    $rules[] = '\'date-' . $column . '\'' . ' => [\'' . $column . '\', \'date\', \'type\' => ' . $this->useClass(DateValidator::class, $tableName) . '::TYPE_TIME, \'timestampAttribute\' => \'' . $column . '\', \'timestampAttributeFormat\' => \'php:H:i:s\']';
                }  else {
                    $rules[] = '\'' . $type . '-' . $column . '\'' . ' => [\'' . $column . '\', \'' . $type . '\']';
                }
            }
        }

        foreach ($lengths as $length => $columns) {
            foreach ($columns as $column) {
                $rules[] = '\'string-' . $column . '\'' . ' => [\'' . $column . '\', \'string\', \'max\' => ' . $length . ']';
            }
        }

        $db = $this->getDbConnection();

        // Unique indexes rules
        try {
            $uniqueIndexes = array_merge($db->getSchema()->findUniqueIndexes($table), [$table->primaryKey]);
            $uniqueIndexes = array_unique($uniqueIndexes, SORT_REGULAR);
            foreach ($uniqueIndexes as $uniqueColumns) {
                // Avoid validating auto incremental columns
                if (!$this->isColumnAutoIncremental($table, $uniqueColumns)) {
                    $attributesCount = count($uniqueColumns);

                    if ($attributesCount === 1) {
                        $rules[] = '\'unique-' . $uniqueColumns[0] . '\'' . ' => [\'' . $uniqueColumns[0] . '\', \'unique\']';
                    } elseif ($attributesCount > 1) {
                        $columnsList = implode("', '", $uniqueColumns);
                        $rules[] = '\'unique-' . implode('-', $uniqueColumns) . '\'' . ' => [[\'' . $columnsList . '\'], \'unique\', \'targetAttribute\' => [' . $columnsList . ']]';
                    }
                }
            }
        } catch (NotSupportedException $e) {
            // doesn't support unique indexes information...do nothing
        }

        // Exist rules for foreign keys
        foreach ($table->foreignKeys as $refs) {
            $refTable = $refs[0];
            $refTableSchema = $db->getTableSchema($refTable);
            $refTableSchema = $this->removeCustomColumns($refTableSchema);

            if ($refTableSchema === null) {
                // Foreign key could point to non-existing table: https://github.com/yiisoft/yii2-gii/issues/34
                continue;
            }

            $refClassName = $this->getTableMeta($refTable)['fullyQualifiedClassName'];

            unset($refs[0]);
            $attributes = implode("', '", array_keys($refs));
            $targetAttributes = [];

            foreach ($refs as $key => $value) {
                if ($this->isIgnoredColumn($key)) {
                    continue 2;
                }

                $targetAttributes[] = "'$key' => '$value'";
            }

            $targetAttributes = implode(', ', $targetAttributes);

            $rules[] = '\'exist-' . implode('-', array_keys($refs)) . '\'' . ' => [[\'' . $attributes . '\'], \'exist\', \'skipOnError\' => true, \'targetClass\' => ' . $this->useClass($refClassName, $tableName) . '::class, \'targetAttribute\' => [' . $targetAttributes . ']]';
        }

        return $rules;
    }

    /**
     * @param TableSchema $table
     * @param array       $fks
     * @param array       $relations
     *
     * @return array
     * @throws Exception
     * @throws InvalidConfigException
     */
    private function generateManyManyRelations(TableSchema $table, array $fks, array $relations): array
    {
        if (!in_array($this->generateJunctionRelationMode, [self::JUNCTION_RELATION_VIA_TABLE, self::JUNCTION_RELATION_VIA_MODEL], true)) {
            throw new InvalidConfigException('Unknown generateViaRelationMode ' . $this->generateJunctionRelationMode);
        }

        $db = $this->getDbConnection();

        foreach ($fks as $pair) {
            [$firstKey, $secondKey] = $pair;
            $table0 = $firstKey[0][0];
            $table1 = $secondKey[0][0];
            unset($firstKey[0][0], $secondKey[0][0]);

            $className0 = $this->getTableMeta($table0)['class'];
            $className1 = $this->getTableMeta($table1)['class'];
            $table0Schema = $db->getTableSchema($table0);
            $table0Schema = $this->removeCustomColumns($table0Schema);

            $table1Schema = $db->getTableSchema($table1);
            $table1Schema = $this->removeCustomColumns($table1Schema);

            // @see https://github.com/yiisoft/yii2-gii/issues/166
            if ($table0Schema === null || $table1Schema === null) {
                continue;
            }

            $link = $this->generateRelationLink(array_flip($secondKey[0]));
            $relationName = $this->generateRelationName($relations, $table0Schema, key($secondKey[0]), true, []);
            if ($this->generateJunctionRelationMode === self::JUNCTION_RELATION_VIA_TABLE) {
                $relations[$table0Schema->fullName][$relationName] = [
                    "return \$this->hasMany($className1::class, $link)->viaTable('"
                    . $this->generateTableName($table->name) . "', " . $this->generateRelationLink($firstKey[0]) . ');',
                    $className1,
                    true,
                    $table0Schema->fullName,
                ];
            } elseif ($this->generateJunctionRelationMode === self::JUNCTION_RELATION_VIA_MODEL) {
                $foreignRelationName = null;
                foreach ($relations[$table0Schema->fullName] as $key => $foreignRelationConfig) {
                    if ($foreignRelationConfig[3] == $firstKey[1]) {
                        $foreignRelationName = $key;
                        break;
                    }
                }
                if (empty($foreignRelationName)) {
                    throw new Exception('Foreign key for junction table not found.');
                }
                $relations[$table0Schema->fullName][$relationName] = [
                    "return \$this->hasMany($className1::class, $link)->via('"
                    . lcfirst($foreignRelationName) . "');",
                    $className1,
                    true,
                    $table0Schema->fullName,
                ];
            }

            $link = $this->generateRelationLink(array_flip($firstKey[0]));
            $relationName = $this->generateRelationName($relations, $table1Schema, key($firstKey[0]), true, []);
            if ($this->generateJunctionRelationMode === self::JUNCTION_RELATION_VIA_TABLE) {
                $relations[$table1Schema->fullName][$relationName] = [
                    "return \$this->hasMany($className0::class, $link)->viaTable('"
                    . $this->generateTableName($table->name) . "', " . $this->generateRelationLink($secondKey[0]) . ');',
                    $className0,
                    true,
                    $table1Schema->fullName,
                ];
            } elseif ($this->generateJunctionRelationMode === self::JUNCTION_RELATION_VIA_MODEL) {
                $foreignRelationName = null;
                foreach ($relations[$table1Schema->fullName] as $key => $foreignRelationConfig) {
                    if ($foreignRelationConfig[3] == $secondKey[1]) {
                        $foreignRelationName = $key;
                        break;
                    }
                }
                if (empty($foreignRelationName)) {
                    throw new Exception('Foreign key for junction table not found.');
                }
                $relations[$table1Schema->fullName][$relationName] = [
                    "return \$this->hasMany($className0::class, $link)->via('"
                    . lcfirst($foreignRelationName) . "');",
                    $className0,
                    true,
                    $table1Schema->fullName,
                ];
            } else {
                throw new InvalidConfigException('Unknown generateViaRelationMode ' . $this->generateJunctionRelationMode);
            }
        }

        return $relations;
    }

    /**
     * @return string[]
     * @throws NotSupportedException
     */
    protected function getSchemaNames(): array
    {
        $db = $this->getDbConnection();

        if ($this->generateRelationsFromCurrentSchema) {
            if ($db->schema->defaultSchema !== null) {
                return [$db->schema->defaultSchema];
            }

            return [''];
        }

        $schema = $db->getSchema();

        if ($schema->hasMethod('getSchemaNames')) { // keep BC to Yii versions < 2.0.4
            try {
                $schemaNames = $schema->getSchemaNames();
            } catch (NotSupportedException $e) {
                // schema names are not supported by schema
            }
        }

        if (!isset($schemaNames)) {
            if (($pos = strpos($this->tableName, '.')) !== false) {
                $schemaNames = [substr($this->tableName, 0, $pos)];
            } else {
                $schemaNames = [''];
            }
        }

        return $schemaNames;
    }

    /**
     * @param TableSchema $table
     * @param array       $fks
     *
     * @return bool
     */
    protected function isHasManyRelation(TableSchema $table, array $fks): bool
    {
        $uniqueKeys = [$table->primaryKey];

        try {
            $uniqueKeys = array_merge($uniqueKeys, $this->getDbConnection()->getSchema()->findUniqueIndexes($table));
        } catch (NotSupportedException $e) {
            // ignore
        }

        foreach ($uniqueKeys as $uniqueKey) {
            if (count(array_diff(array_merge($uniqueKey, $fks), array_intersect($uniqueKey, $fks))) === 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $refs
     *
     * @return string
     */
    protected function generateRelationLink(array $refs): string
    {
        $pairs = [];
        foreach ($refs as $a => $b) {
            $pairs[] = "'$a' => '$b'";
        }

        return '[' . implode(', ', $pairs) . ']';
    }

    /**
     * Checks if the given table is a junction table, that is it has at least one pair of unique foreign keys.
     *
     * @param TableSchema the table being checked
     *
     * @return array|bool all unique foreign key pairs if the table is a junction table,
     * or false if the table is not a junction table.
     */
    protected function checkJunctionTable($table)
    {
        if (count($table->foreignKeys) < 2) {
            return false;
        }
        $uniqueKeys = [$table->primaryKey];
        try {
            $uniqueKeys = array_merge($uniqueKeys, $this->getDbConnection()->getSchema()->findUniqueIndexes($table));
        } catch (NotSupportedException $e) {
            // ignore
        }
        $result = [];
        // find all foreign key pairs that have all columns in an unique constraint
        $foreignKeyNames = array_keys($table->foreignKeys);
        $foreignKeys = array_values($table->foreignKeys);
        $foreignKeysCount = count($foreignKeys);

        for ($i = 0; $i < $foreignKeysCount; $i++) {
            $firstColumns = $foreignKeys[$i];
            unset($firstColumns[0]);

            for ($j = $i + 1; $j < $foreignKeysCount; $j++) {
                $secondColumns = $foreignKeys[$j];
                unset($secondColumns[0]);

                $fks = array_merge(array_keys($firstColumns), array_keys($secondColumns));
                foreach ($uniqueKeys as $uniqueKey) {
                    if (count(array_diff(array_merge($uniqueKey, $fks), array_intersect($uniqueKey, $fks))) === 0) {
                        // save the foreign key pair
                        $result[] = [
                            [
                                $foreignKeys[$i],
                                $table->fullName . '.' . $foreignKeyNames[$i]
                            ],
                            [
                                $foreignKeys[$j],
                                $table->fullName . '.' . $foreignKeyNames[$j]
                            ]
                        ];
                        break;
                    }
                }
            }
        }
        return empty($result) ? false : $result;
    }

    /**
     * Generate a relation name for the specified table and a base name.
     *
     * @param array       $relations the relations being generated currently.
     * @param TableSchema $table     the table schema
     * @param string      $key       a base name that the relation name may be generated from
     * @param bool        $multiple  whether this is a has-many relation
     *
     * @return string the relation name
     * @throws \ReflectionException
     */
    protected function generateRelationName(array $relations, TableSchema $table, string $key, bool $multiple, array $refs): string
    {
        static $baseModel;
        /* @var $baseModel \yii\db\ActiveRecord */
        if ($baseModel === null) {
            $baseClass = $this->baseClass;
            $baseClassReflector = new \ReflectionClass($baseClass);
            if ($baseClassReflector->isAbstract()) {
                $baseClassWrapper =
                    'namespace ' . __NAMESPACE__ . ';'.
                    'class GiiBaseClassWrapper extends \\' . $baseClass . ' {' .
                    'public static function tableName(){' .
                    'return "' . addslashes($table->fullName) . '";' .
                    '}' .
                    '};' .
                    'return new GiiBaseClassWrapper();';
                $baseModel = eval($baseClassWrapper);
            } else {
                $baseModel = new $baseClass();
            }
            $baseModel->setAttributes([]);
        }

        if (!empty($key) && strcasecmp($key, 'id')) {
            if (substr_compare($key, 'id', -2, 2, true) === 0) {
                $key = rtrim(substr($key, 0, -2), '_');
            } elseif (substr_compare($key, 'id_', 0, 3, true) === 0) {
                $key = ltrim(substr($key, 3, strlen($key)), '_');
            }
        }

        if ($multiple) {
            $key = Inflector::pluralize($key);
        }

        $name = $rawName = Inflector::id2camel($key, '_');

        if ($multiple) {
            foreach (array_keys($refs) as $ref) {
                $name = Inflector::id2camel(str_replace([
                    '_id',
                    '_by'
                ], '', $ref), '_') . $name;
            }
        }

        $i = 0;
        while ($baseModel->hasProperty(lcfirst($name))) {
            $name = $rawName . ($i++);
        }
        while (isset($table->columns[lcfirst($name)])) {
            $name = $rawName . ($i++);
        }
        while (isset($relations[$table->fullName][$name])) {
            $name = $rawName . ($i++);
        }

        return $name;
    }

    /**
     * Validates the [[db]] attribute.
     */
    public function validateDb(): void
    {
        if (!Yii::$app->has($this->db)) {
            $this->addError('db', 'There is no application component named "db".');
        } elseif (!Yii::$app->get($this->db) instanceof Connection) {
            $this->addError('db', 'The "db" application component must be a DB connection instance.');
        }
    }

    /**
     * Validates the namespace.
     *
     * @param string $attribute Namespace variable.
     */
    public function validateNamespace($attribute)
    {
        $value = $this->$attribute;
        $value = ltrim($value, '\\');
        $path = Yii::getAlias('@' . str_replace('\\', '/', $value), false);
        if ($path === false) {
            $this->addError($attribute, 'Namespace must be associated with an existing directory.');
        }
    }

    /**
     * Validates the [[modelClass]] attribute.
     */
    public function validateModelClass()
    {
        if ($this->isReservedKeyword($this->modelClass)) {
            $this->addError('modelClass', 'Class name cannot be a reserved PHP keyword.');
        }
        if ((empty($this->tableName) || substr_compare($this->tableName, '*', -1, 1)) && $this->modelClass == '') {
            $this->addError('modelClass', 'Model Class cannot be blank if table name does not end with asterisk.');
        }
    }

    protected $tableNames;
    protected $classNames;

    /**
     * @return array the table names that match the pattern specified by [[tableName]].
     */
    protected function getTableNames()
    {
        if ($this->tableNames !== null) {
            return $this->tableNames;
        }

        $db = $this->getDbConnection();

        if ($db === null) {
            return [];
        }

        $tableNames = [];

        if (strpos($this->tableName, '*') !== false) {
            if (($pos = strrpos($this->tableName, '.')) !== false) {
                $schema = substr($this->tableName, 0, $pos);
                $pattern = '/^' . str_replace('*', '\w+', substr($this->tableName, $pos + 1)) . '$/';
            } else {
                $schema = '';
                $pattern = '/^' . str_replace('*', '\w+', $this->tableName) . '$/';
            }

            foreach ($db->schema->getTableNames($schema) as $table) {
                if (preg_match($pattern, $table)) {
                    $tableNames[] = $schema === '' ? $table : ($schema . '.' . $table);
                }
            }
        } elseif ($db->getTableSchema($this->tableName, true) !== null) {
            $tableNames[] = $this->tableName;
            $this->classNames[$this->tableName] = $this->modelClass;
        }

        return $this->tableNames = $tableNames;
    }

    /**
     * Generates the table name by considering table prefix.
     * If [[useTablePrefix]] is false, the table name will be returned without change.
     * @param string $tableName the table name (which may contain schema prefix)
     * @return string the generated table name
     */
    public function generateTableName($tableName)
    {
        $tableMeta = $this->getTableMeta($tableName);

        return '{{%' . $tableMeta['tableName'] . '}}';
    }

    /**
     * @return Connection the DB connection as specified by [[db]].
     * @throws InvalidConfigException
     */
    protected function getDbConnection(): Connection
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return Yii::$app->get($this->db, false);
    }

    /**
     * @return string|null driver name of db connection.
     * In case db is not instance of \yii\db\Connection null will be returned.
     * @since 2.0.6
     */
    protected function getDbDriverName(): ?string
    {
        /** @var Connection $db */
        $db = $this->getDbConnection();
        return $db instanceof \yii\db\Connection ? $db->driverName : null;
    }

    /**
     * Checks if any of the specified columns is auto incremental.
     *
     * @param TableSchema $table   the table schema
     * @param array               $columns columns to check for autoIncrement property
     *
     * @return bool whether any of the specified columns is auto incremental.
     */
    protected function isColumnAutoIncremental(TableSchema $table, array $columns): bool
    {
        foreach ($columns as $column) {
            if (isset($table->columns[$column]) && $table->columns[$column]->autoIncrement) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $classWithNamespace
     * @param string $tableName
     *
     * @return string
     */
    public function useClass(string $classWithNamespace, string $tableName): string
    {
        if ($this->useRealClasses) {
            $classMap = array_flip(Yii::$app->injector->classMap);
            $classWithNamespace = $classMap[$classWithNamespace] ?? $classWithNamespace;
        }

        $classWithoutNamespace = $this->getClassWithoutNamespace($classWithNamespace);

        if ($classWithNamespace === $this->generatedModelClass) {
            return 'static';
        }

        $this->usedClasses[$tableName][] = $classWithNamespace;
        return $classWithoutNamespace;
    }

    /**
     * @param array $classesWithNamespace
     * @param bool  $useKey
     *
     * @return array
     */
    private function getClassesWithoutNamespace(array $classesWithNamespace, bool $useKey = false): array
    {
        $classes = [];

        foreach ($classesWithNamespace as $key => $value) {
            if ($useKey) {
                $class = $this->getClassWithoutNamespace($key);
            } else {
                $class = $this->getClassWithoutNamespace($value);
            }

            if ($useKey) {
                $classes[$class] = $value;
            } else {
                $classes[] = $class;
            }
        }

        return $classes;
    }

    /**
     * @param string $classWithNamespace
     *
     * @return string
     */
    private function getClassWithoutNamespace(string $classWithNamespace): string
    {
        return substr($classWithNamespace, strrpos($classWithNamespace, '\\') + 1);
    }

    private array $tableModules = [];

    /**
     * @param string    $fullTableName
     * @param bool|null $useSchemaName
     *
     * @return array|null
     * @throws InvalidConfigException
     */
    protected function getTableMeta(string $fullTableName, ?bool $useSchemaName = null): ?array
    {
        if (!isset($this->tableModules[$fullTableName])) {
            $tableName = $fullTableName;

            $tablePrefixes = [];

            foreach (Yii::$app->fafcms->loadedPlugins as $loadedPlugin) {
                $tablePrefixes[] = [
                    'prefix' => $loadedPlugin['bootstrap']::getTablePrefix(),
                    'namespace' => substr($loadedPlugin['module'], 0, -6),
                    'module' => $loadedPlugin['module'],
                    'bootstrap' => $loadedPlugin['bootstrap'],
                    'id' => $loadedPlugin['bootstrap']::$id
                ];
            }

            $tablePrefixes = array_values(array_filter($tablePrefixes, static function($tablePrefix) use ($tableName) {
                return mb_strpos($tableName, $tablePrefix['prefix']) === 0;
            }));

            if (isset($tablePrefixes[0])) {
                $tableName = str_replace($tablePrefixes[0]['prefix'], '', $tableName);
            }

            $schemaName = '';

            if (($pos = strrpos($tableName, '.')) !== false) {
                if (($useSchemaName === null && $this->useSchemaName) || $useSchemaName) {
                    $schemaName = substr($tableName, 0, $pos) . '_';
                }
                $tableName = substr($tableName, $pos + 1);
            }

            $db = $this->getDbConnection();
            $patterns = [];
            $patterns[] = "/^{$db->tablePrefix}(.*?)$/";
            $patterns[] = "/^(.*?){$db->tablePrefix}$/";

            if (strpos($this->tableName, '*') !== false) {
                $pattern = $this->tableName;
                if (($pos = strrpos($pattern, '.')) !== false) {
                    $pattern = substr($pattern, $pos + 1);
                }
                $patterns[] = '/^' . str_replace('*', '(\w+)', $pattern) . '$/';
            }

            $className = $tableName;

            foreach ($patterns as $pattern) {
                if (preg_match($pattern, $tableName, $matches)) {
                    $className = $matches[1];
                    break;
                }
            }

            $tableModelClass = Inflector::id2camel($schemaName . $className, '_');

            $tableMeta = [
                'namespace' => $this->ns,
                'abstract-namespace' => $this->abstractNs,
                'class' => $tableModelClass,
                'tableName' => $tableName,
                'fullTableName' => $fullTableName,
                'messageCategory' => explode('\\', $this->ns)[0]
            ];

            if (($modulePrefix = $tablePrefixes[0] ?? null) !== null) {
                $tableMeta['namespace'] = $modulePrefix['namespace'] . 'models';
                $tableMeta['abstract-namespace'] = $modulePrefix['namespace'] . 'abstracts\\models';
                $tableMeta['module'] = $modulePrefix['module'];
                $tableMeta['bootstrap'] = $modulePrefix['bootstrap'];
                $tableMeta['prefix'] = $modulePrefix['prefix'];
                $tableMeta['messageCategory'] = $modulePrefix['id'];
            }

            $tableMeta['namespace'] = rtrim($tableMeta['namespace'], '\\');
            $tableMeta['fullyQualifiedClassName'] = $tableMeta['namespace'] . '\\' . $tableMeta['class'];
            $this->tableModules[$fullTableName] = $tableMeta;
        }

        return $this->tableModules[$fullTableName];
    }

    /**
     * @param string $tableName
     *
     * @return string
     */
    public function getSortedUsedClasses(string $tableName): string
    {
        $usedClasses = array_unique($this->usedClasses[$tableName]);
        $classGroups = [];

        foreach ($usedClasses as $usedClass) {
            $classParts = explode('\\', $usedClass);
            $partCount = count($classParts);

            if ($partCount === 1) {
                $classGroups[$classParts[0]][] = '';
            } elseif ($partCount === 2) {
                $classGroups[$classParts[0]][] = $classParts[1];
            } elseif ($partCount === 3) {
                $classGroups[$classParts[0] . '\\' . $classParts[1]][] = $classParts[2];
            } elseif ($partCount > 3) {
                $groupParts = array_splice($classParts, -2);

                $classGroups[implode('\\', $classParts)][] = implode('\\', $groupParts);
            }
        }

        $classString = '';

        ksort($classGroups,   SORT_STRING | SORT_FLAG_CASE);

        foreach ($classGroups as $classGroup => $groupItems) {
            if (count($groupItems) > 1) {
                $classString .= 'use '.$classGroup.'\\{'."\n";

                sort($groupItems,   SORT_STRING | SORT_FLAG_CASE);

                foreach ($groupItems as $i => $groupItem) {
                    $classString .= '    '.$groupItem.','."\n";
                }

                $classString .= '};'."\n";
            } else {
                $classString .= 'use '.$classGroup.($groupItems[0] !== ''?'\\'.$groupItems[0]:'').';'."\n";
            }
        }

        return $classString;
    }

    /**
     * @param TableSchema $tableSchema
     *
     * @return string[]|null
     */
    public function getPrimaryColumns(TableSchema $tableSchema): ?array
    {
        $columnNames = ArrayHelper::getColumn($tableSchema->columns, 'name');

        foreach ($this->possiblePrimaryColumns as $possiblePrimaryColumns) {
            if (!is_array($possiblePrimaryColumns)) {
                $possiblePrimaryColumns = [$possiblePrimaryColumns];
            }

            foreach ($possiblePrimaryColumns as $possiblePrimaryColumn) {
                if (!isset($columnNames[$possiblePrimaryColumn])) {
                    continue 2;
                }
            }

            return $possiblePrimaryColumns;
        }

        return null;
    }
}
