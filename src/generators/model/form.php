<?php

use fafcms\dev\generators\model\Generator;

/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator fafcms\dev\generators\model\Generator */

echo $form->field($generator, 'tableName')->textInput(['table_prefix' => $generator->getTablePrefix()]);
echo $form->field($generator, 'ns');
echo $form->field($generator, 'abstractNs');
echo $form->field($generator, 'baseClass');
echo $form->field($generator, 'db');
echo $form->field($generator, 'generateJunctionRelationMode')->dropDownList([
    Generator::JUNCTION_RELATION_VIA_TABLE => 'Via Table',
    Generator::JUNCTION_RELATION_VIA_MODEL => 'Via Model',
]);
echo $form->field($generator, 'generateRelationsFromCurrentSchema')->checkbox();
echo $form->field($generator, 'useSchemaName')->checkbox();
echo $form->field($generator, 'useRealClasses')->checkbox();
echo $form->field($generator, 'ignoreColumns')->textInput();
echo $form->field($generator, 'ignoreNamespaces')->textInput();
