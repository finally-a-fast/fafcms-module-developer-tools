[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | <?= $generator->title . PHP_EOL ?>
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added


### Changed


### Fixed


[Unreleased]: https://gitlab.com/<?= $generator->vendorName ?>/<?= $generator->packageName ?>/-/tree/master
