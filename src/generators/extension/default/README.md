<?php if ($generator->vendorName === 'finally-a-fast'): ?>[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | <?php endif; ?>Readme | <?= $generator->title . PHP_EOL ?>
================================================

<?php if ($generator->vendorName === 'finally-a-fast'): ?>
[![Latest Stable Version](https://img.shields.io/packagist/v/<?= $generator->vendorName ?>/<?= $generator->packageName ?>?label=stable&style=flat-square)](https://packagist.org/packages/<?= $generator->vendorName ?>/<?= $generator->packageName ?>)
[![Latest Version](https://img.shields.io/packagist/v/<?= $generator->vendorName ?>/<?= $generator->packageName ?>?include_prereleases&label=unstable&style=flat-square)](https://packagist.org/packages/<?= $generator->vendorName ?>/<?= $generator->packageName ?>)
[![PHP Version](https://img.shields.io/packagist/php-v/<?= $generator->vendorName ?>/<?= $generator->packageName ?>/dev-master?style=flat-square)](https://www.php.net/downloads.php)
[![License](https://img.shields.io/packagist/l/<?= $generator->vendorName ?>/<?= $generator->packageName ?>?style=flat-square)](https://packagist.org/packages/<?= $generator->vendorName ?>/<?= $generator->packageName ?>)
[![Total Downloads](https://img.shields.io/packagist/dt/<?= $generator->vendorName ?>/<?= $generator->packageName ?>?style=flat-square)](https://packagist.org/packages/<?= $generator->vendorName ?>/<?= $generator->packageName ?>)
[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat-square)](http://www.yiiframework.com/)
<?php endif; ?>


<?= $generator->description ?>

Installation
------------

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run
```
php composer.phar require <?= $generator->vendorName ?>/<?= $generator->packageName . PHP_EOL ?>
```
or add
```
"<?= $generator->vendorName ?>/<?= $generator->packageName ?>": "dev-master"
```
to the require section of your `composer.json` file.

<?php if ($generator->vendorName === 'finally-a-fast'): ?>
Documentation
------------

[Documentation](https://www.finally-a-fast.com/) will be added soon at https://www.finally-a-fast.com/.

<?php endif; ?>
License
-------

**<?= $generator->packageName ?>** is released under the MIT License. See the [LICENSE.md](LICENSE.md) for details.
