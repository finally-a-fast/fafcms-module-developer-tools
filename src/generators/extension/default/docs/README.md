[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Docs | <?= $generator->title . PHP_EOL ?>
============================

API Documentation
-----------------
[English](api/index.html)

Guide
-----
[English](guide/en/README.md)

[Deutsch](guide/de/README.md)
