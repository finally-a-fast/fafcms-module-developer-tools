<?= "<?php\n" ?>

declare(strict_types=1);

<?php if ($generator->vendorName === 'finally-a-fast'): ?>
/**
 * @author <?= $generator->authorName ?> <<?= $generator->authorEmail ?>>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/<?= $generator->packageName ?>/license MIT
 * @link https://www.finally-a-fast.com/packages/<?= $generator->packageName . PHP_EOL ?>
 * @see https://www.finally-a-fast.com/packages/<?= $generator->packageName ?>/docs Documentation of the <?= strtolower($generator->title) . PHP_EOL ?>
 * @since File available since Release 1.0.0
 */

<?php endif; ?>
namespace <?= substr($generator->namespace, 0, -1) ?>;

use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;

/**
 * Class Bootstrap
 *
 * @package <?= substr($generator->namespace, 0, -1) . PHP_EOL ?>
 */
class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = '<?= ($generator->vendorName === 'finally-a-fast' ? str_replace('-module-',  '-', $generator->packageName) : $generator->packageName) ?>';
    public static $tablePrefix = '<?= ($generator->vendorName === 'finally-a-fast' ? str_replace('manager',  '', str_replace('-module-',  '-', $generator->packageName)) : substr($generator->packageName, 0, 18)) ?>_';

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['<?= ($generator->vendorName === 'finally-a-fast' ? str_replace('-module-',  '-', $generator->packageName) : $generator->packageName) ?>'])) {
            $app->i18n->translations['<?= ($generator->vendorName === 'finally-a-fast' ? str_replace('-module-',  '-', $generator->packageName) : $generator->packageName) ?>'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    protected function bootstrapWebApp(Application $app, PluginModule $module): bool
    {
        return true;
    }
}
