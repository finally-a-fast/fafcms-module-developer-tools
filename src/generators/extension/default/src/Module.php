<?= "<?php\n" ?>

declare(strict_types=1);

<?php if ($generator->vendorName === 'finally-a-fast'): ?>
/**
 * @author <?= $generator->authorName ?> <<?= $generator->authorEmail ?>>
 * @copyright Copyright (c) 2019 - 2021 Finally a fast
 * @license https://www.finally-a-fast.com/packages/<?= $generator->packageName ?>/license MIT
 * @link https://www.finally-a-fast.com/packages/<?= $generator->packageName . PHP_EOL ?>
 * @see https://www.finally-a-fast.com/packages/<?= $generator->packageName ?>/docs Documentation of the <?= strtolower($generator->title) . PHP_EOL ?>
 * @since File available since Release 1.0.0
 */

<?php endif; ?>
namespace <?= substr($generator->namespace, 0, -1) ?>;

use Yii;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\helpers\classes\PluginSetting;
use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\inputs\Checkbox;

/**
 * Class Module
 *
 * @package <?= substr($generator->namespace, 0, -1) . PHP_EOL ?>
 */
class Module extends PluginModule
{
    public function getPluginSettingRules(): array
    {
        return [
            [['example_setting'], 'boolean'],
        ];
    }

    protected function pluginSettingDefinitions(): array
    {
        return [
            new PluginSetting($this, [
                'name' => 'example_setting',
                'label' => Yii::t('<?= ($generator->vendorName === 'finally-a-fast' ? str_replace('-module-',  '-', $generator->packageName) : $generator->packageName) ?>', 'Example setting'),
                'defaultValue' => true,
                'inputType' => Checkbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
            ]),
        ];
    }
}
