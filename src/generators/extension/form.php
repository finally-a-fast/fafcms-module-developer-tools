<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator yii\gii\generators\extension\Generator */

?>
<div class="module-form">
<?php
echo $form->field($generator, 'title');
echo $form->field($generator, 'packageName');
echo $form->field($generator, 'vendorName');
echo $form->field($generator, 'namespace');
echo $form->field($generator, 'description');
echo $form->field($generator, 'keywords');
echo $form->field($generator, 'license')->dropDownList($generator->optsLicense(), ['prompt'=>'Choose...']);
echo $form->field($generator, 'authorName');
echo $form->field($generator, 'authorEmail');
echo $form->field($generator, 'outputPath');
?>
</div>
