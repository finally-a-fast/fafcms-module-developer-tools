<?php

namespace fafcms\dev;

use fafcms\fafcms\components\FafcmsComponent;
use yii\base\Application;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;
use Yii;

class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-dev';
    public static $tablePrefix = 'fafcms-dev_';

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['fafcms-dev'])) {
            $app->i18n->translations['fafcms-dev'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    protected function bootstrapApp(Application $app, PluginModule $module): bool
    {
        if (YII_CONSOLE || (!Yii::$app->getUser()->getIsGuest() && Yii::$app->user->can('accessBackend'))) {
            Yii::$app->setModule('gii', [
                'class' => 'fafcms\dev\gii\Module',
            ]);

            $gii = $app->getModule('gii');
            $gii->bootstrap($app);

            Yii::$app->view->addNavigationItems(
                FafcmsComponent::NAVIGATION_SIDEBAR_LEFT,
                [
                    'system' => [
                        'items' => [
                            'developer'    => [
                                'after' => 'settings',
                                'icon' => 'developer-board',
                                'label' => Yii::t('fafcms-dev', 'Developer'),
                                'items' => [
                                    'gii' => [
                                        'icon' => 'puzzle-edit-outline',
                                        'label' => Yii::t('fafcms-dev', 'Code generator'),
                                        'url' => ['/gii/default/index']
                                    ],
                                    'cache' => [
                                        'icon' => 'puzzle-edit-outline',
                                        'label' => Yii::t('fafcms-dev', 'Cache'),
                                        'url' => ['/main/cache']
                                    ],
                                ],
                            ],
                        ],
                    ],
                ]
            );
        }

        return true;
    }
}
