<?php

namespace fafcms\dev\gii;

use Yii;
use yii\base\BootstrapInterface;
use yii\helpers\Json;
use yii\web\ForbiddenHttpException;

/**
 * Class Module
 *
 * @package fafcms\dev
 */
class Module extends \yii\gii\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'fafcms\dev\gii\controllers';

    /**
     * {@inheritdoc}
     */
    public function bootstrap($app)
    {
        if ($app instanceof \yii\web\Application) {
            $app->fafcms->addBackendUrlRules('gii', [
                '' => 'default/index',
                '<id:\w+>' => 'default/view',
                '<controller:[\w\-]+>/<action:[\w\-]+>' => '<controller>/<action>',
            ]);
        } elseif ($app instanceof \yii\console\Application) {
            $app->controllerMap[$this->id] = [
                'class' => 'yii\gii\console\GenerateController',
                'generators' => array_merge($this->coreGenerators(), $this->generators),
                'module' => $this,
            ];
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function resetGlobalSettings()
    {
    }

    /**
     * {@inheritdoc}
     */
    protected function checkAccess(): bool
    {
        if (YII_CONSOLE || (!Yii::$app->getUser()->getIsGuest() && Yii::$app->user->can('accessBackend'))) {
            //TODO add extra rule for code generator
            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function coreGenerators()
    {
        return [
            'job' => [
                'class' => \fafcms\dev\generators\job\Generator::class,
            ],
            'model' => [
                'class' => \fafcms\dev\generators\model\Generator::class,
            ],
            'extension' => [
                'class' => \fafcms\dev\generators\extension\Generator::class,
            ],
            'controller' => [
                'class' => \fafcms\dev\generators\controller\Generator::class,
            ],
        ];
    }
}
