<?php
use yii\helpers\Html;

/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $generators \yii\gii\Generator[] */
/* @var $content string */

$generators = Yii::$app->controller->module->generators;

$items = [];

foreach ($generators as $id => $generator) {
    $items[] = [
        'options' => ['class' => ['column sixteen wide mobile eight wide tablet four wide computer four wide large screen four wide widescreen']],
        'content' => $this->render('@fafcms/fafcms/views/common/card', [
            'title' => Html::encode($generator->getName()),
            'content' => $generator->getDescription(),
            'buttons' => [[
                'label' => 'Start',
                'icon' => $generator->icon ?? 'file-code-outline',
                'url' => ['default/view', 'id' => $id],
            ]]
        ]),
    ];
}

echo $this->render('@fafcms/fafcms/views/common/base', [
    'content' => $this->render('@fafcms/fafcms/views/common/grid', [
        'rows' => [$items],
    ]),
]);
