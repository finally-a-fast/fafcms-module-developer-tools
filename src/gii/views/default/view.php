<?php

use yii\helpers\Html;
use fafcms\fafcms\widgets\ActiveForm;
use fafcms\dev\components\ActiveField;
use yii\gii\CodeFile;
use fafcms\dev\assets\GeneratorAsset;

/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $generator yii\gii\Generator */
/* @var $id string panel ID */
/* @var $form yii\widgets\ActiveForm */
/* @var $results string */
/* @var $hasError bool */
/* @var $files CodeFile[] */
/* @var $answers array */

$asset = GeneratorAsset::register($this);
$templates = [];

foreach ($generator->templates as $name => $path) {
    $templates[$name] = "$name ($path)";
}

echo '<div class="default-view">';

$form = ActiveForm::begin([
    'id' => $id . '-generator',
    'validationStateOn' => ActiveForm::VALIDATION_STATE_ON_INPUT,
    'fieldConfig' => [
        'class' => ActiveField::className(),
    ],
]);

$content = '<p>' . $generator->getDescription() . '</p><br>';

$content .= $this->renderFile($generator->formView(), [
    'generator' => $generator,
    'form' => $form,
]);

$content .= $form->field($generator, 'template')
    ->sticky()
    ->hint('Please select which set of the templates should be used to generated the code.')
    ->label('Code Template')
    ->dropDownList($templates);

$content = $this->render('@fafcms/fafcms/views/common/card', [
    'title' => Html::encode($generator->getName()),
    'content' => $content,
    'buttons' => [[
        'label' => 'Preview',
        'type' => 'submit',
        'options' => [
            'name' => 'preview'
        ]
    ]]
]);

if (isset($results)) {
    $content .= $this->render('@fafcms/fafcms/views/common/card', [
        'title' => 'Result',
        'content' => $this->render('view/results', [
            'generator' => $generator,
            'results' => $results,
            'hasError' => $hasError,
        ]),
    ]);
} elseif (isset($files)) {
    $content .= $this->render('@fafcms/fafcms/views/common/card', [
        'title' => 'Files to generate',
        'content' => $this->render('view/files', [
            'id' => $id,
            'generator' => $generator,
            'files' => $files,
            'answers' => $answers,
            'form' => $form
        ]),
        'buttons' => [[
            'label' => 'Generate',
            'type' => 'submit',
            'options' => [
                'name' => 'generate', 'class' => 'primary'
            ],
        ]]
    ]);
}

echo $this->render('@fafcms/fafcms/views/common/base', [
    'content' => $this->render('@fafcms/fafcms/views/common/grid', [
        'rows' => [[[
            'options' => ['class' => ['column']],
            'content' => $content,
        ]]],
    ]),
]);

ActiveForm::end();

echo '</div>';
