<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $generator yii\gii\Generator */
/* @var $results string */
/* @var $hasError bool */
?>
<div class="default-view-results">
    <?php
    if ($hasError) {
        echo '<div class="ui negative message"><p>There was something wrong when generating the code. Please check the following messages.</p></div>';
    } else {
        echo '<div class="ui positive message"><p>' . $generator->successMessage() . '</p></div>';
    }
    ?>
    <div class="ui message">
        <div class="header">
            Action log
        </div>
        <p>
            <?= nl2br($results) ?>
        </p>
    </div>
</div>
