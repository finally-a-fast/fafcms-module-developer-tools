<?php

use yii\gii\CodeFile;
use yii\helpers\Html;

/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $generator \yii\gii\Generator */
/* @var $files CodeFile[] */
/* @var $answers array */
/* @var $id string panel ID */
/* @var $form fafcms\fafcms\widgets\ActiveForm */
?>
<div class="default-view-files">
    <div class="ui grid compact stretched">
        <div class="row">
            <div class="column sixteen wide mobile sixteen wide tablet eight wide computer ten wide large screen twelve wide widescreen">
                <div class="field">
                    <label for="filter-input">Type to filter</label>
                    <input type="text" id="filter-input" class="field">
                </div>
            </div>
            <div class="column sixteen wide mobile sixteen wide tablet eight wide computer six wide large screen four wide widescreen bottom aligned">
                <div id="action-toggle" class="inline fields">
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox" value="<?= CodeFile::OP_CREATE ?>" checked>
                            <label class="popup-hover" data-content="Filter files that are created." data-position="top right">Create</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox" value="<?= CodeFile::OP_SKIP ?>" checked>
                            <label class="popup-hover" data-content="Filter files that are unchanged." data-position="top right">Unchanged</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox" value="<?= CodeFile::OP_OVERWRITE ?>" checked>
                            <label class="popup-hover" data-content="Filter files that are overwritten." data-position="top right">Overwrite</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <table class="ui celled table">
        <thead>
            <tr>
                <?php
                $fileChangeExists = false;
                foreach ($files as $file) {
                    if ($file->operation !== CodeFile::OP_SKIP) {
                        $fileChangeExists = true;
                        echo '<th><div class="ui checkbox"><input type="checkbox" id="check-all"><label></label></div></th>';
                        break;
                    }
                }
                ?>
                <th>File</th>
                <th>Action</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="files-body">
            <?php foreach ($files as $file): ?>
                <?php
                if ($file->operation === CodeFile::OP_OVERWRITE) {
                    $trClass = 'warning';
                } elseif ($file->operation === CodeFile::OP_SKIP) {
                    $trClass = 'info';
                } elseif ($file->operation === CodeFile::OP_CREATE) {
                    $trClass = 'positive';
                } else {
                    $trClass = '';
                }
                ?>
            <tr class="<?= "$file->operation $trClass" ?>">
                <?php if ($fileChangeExists): ?>
                    <td class="check">
                        <?php
                        if ($file->operation === CodeFile::OP_SKIP) {
                            echo '&nbsp;';
                        } else {
                            echo Html::tag(
                                'div',
                                Html::checkBox("answers[{$file->id}]", isset($answers) ? isset($answers[$file->id]) : ($file->operation === CodeFile::OP_CREATE)) . '<label class="popup-hover"></label>',
                                ['class' => 'ui checkbox']
                            );
                        }
                        ?>
                    </td>
                <?php endif; ?>
                <td>
                    <?= Html::a(Html::encode($file->getRelativePath()), ['preview', 'id' => $id, 'file' => $file->id], ['class' => 'preview-code', 'data-title' => $file->getRelativePath()]) ?>
                </td>
                <td>
                    <?php
                    if ($file->operation === CodeFile::OP_SKIP) {
                        echo 'unchanged';
                    } else {
                        echo $file->operation;
                    }
                    ?>
                </td>
                <td>
                    <div class="ui icon buttons">
                        <?php if ($file->operation === CodeFile::OP_OVERWRITE): ?>
                            <?= Html::a('<i class="icon mdi mdi-compare-horizontal"></i>', ['diff', 'id' => $id, 'file' => $file->id], ['class' => 'diff-code ui button icon', 'data-title' => $file->getRelativePath()]) ?>
                        <?php endif; ?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="ui modal fullscreen" id="preview-modal">
        <header class="ui inverted header modal-header" style="display:flex">
            <h4 class="modal-title">Modal title</h4>
            <div class="ui buttons mini">
                <a class="modal-previous ui button icon" href="#" title="Previous File (Left Arrow)"><i class="icon mdi mdi-arrow-left"></i></a>
                <a class="modal-next ui button icon" href="#" title="Next File (Right Arrow)"><i class="icon mdi mdi-arrow-right"></i></a>
                <a class="modal-refresh ui button icon" href="#" title="Refresh File (R)"><i class="icon mdi mdi-refresh"></i></a>
                <button class="modal-checkbox ui button icon" title="Check This File (Space)"><i class="icon mdi mdi-checkbox-blank-outline"></i></button>
                <button class="ui button icon copy-text" data-copy-target="#clipboard" title="Copy file to clipboard"><i class="mdi mdi-content-copy"></i></button>
            </div>
            <div id="clipboard-container"><textarea id="clipboard"></textarea></div>
        </header>
        <i class="close icon inside"></i>
        <div class="scrolling content modal-body">
            <p>Please wait ...</p>
        </div>
    </div>
</div>
