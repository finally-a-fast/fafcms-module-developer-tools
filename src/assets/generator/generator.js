yii.gii = (function ($) {
    var ajaxRequest = null

    var fillModal = function($link, data) {
        var $modal = $('#preview-modal'),
         $modalBody = $modal.find('.modal-body');
        if (!$link.hasClass('modal-refresh')) {
            var filesSelector = 'a.' + $modal.data('action') + ':visible';
            var $files = $(filesSelector);
            var index = $files.filter('[href="' + $link.attr('href') + '"]').index(filesSelector);
            var $prev = $files.eq(index - 1);
            var $next = $files.eq((index + 1 == $files.length ? 0 : index + 1));
            $modal.data('current', $files.eq(index));
            $modal.find('.modal-previous').attr('href', $prev.attr('href')).data('title', $prev.data('title'));
            $modal.find('.modal-next').attr('href', $next.attr('href')).data('title', $next.data('title'));
        }
        $modalBody.html(data);
        var valueToCopy = $("<div/>").html(data.replace(/(<(br[^>]*)>)/ig, '\n').replace(/&nbsp;/ig, ' ')).text().trim() + '\n'; //TODO
        $('#clipboard').val(valueToCopy)

        $modal.find('.content').css('max-height', ($(window).height() - 200) + 'px');
    };

    var initPreviewDiffLinks = function () {
        $('.preview-code, .diff-code, .modal-refresh, .modal-previous, .modal-next').on('click', function () {
            if (ajaxRequest !== null) {
                if ($.isFunction(ajaxRequest.abort)) {
                    ajaxRequest.abort();
                }
            }
            var that = this;
            var $modal = $('#preview-modal');
            var $link = $(this);
            $modal.find('.modal-refresh').attr('href', $link.attr('href'));
            if ($link.hasClass('preview-code') || $link.hasClass('diff-code')) {
                $modal.data('action', ($link.hasClass('preview-code') ? 'preview-code' : 'diff-code'))
            }
            $modal.find('.modal-title').text($link.data('title'));
            $modal.find('.modal-body').html('Loading ...');

            var modalInitJs = $modal.modal().modal('show');

            var checkbox = $('a.' + $modal.data('action') + '[href="' + $link.attr('href') + '"]').closest('tr').find('input').get(0);
            var checked = false;

            if (checkbox) {
                checked = checkbox.checked;
                $modal.find('.modal-checkbox').removeClass('disabled');
            } else {
                $modal.find('.modal-checkbox').addClass('disabled');
            }

            $modal.find('.modal-checkbox').toggleClass('checked', checked).toggleClass('unchecked', !checked);

            ajaxRequest = $.ajax({
                type: 'POST',
                cache: false,
                url: $link.prop('href'),
                data: $('.default-view form').serializeArray(),
                success: function (data) {
                    fillModal($(that), data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $modal.find('.modal-body').html('<div class="error">' + XMLHttpRequest.responseText + '</div>');
                }
            });
            return false;
        });

        $('#preview-modal').on('keydown', function (e) {
            if (e.keyCode === 37) {
                $('.modal-previous').trigger('click');
            } else if (e.keyCode === 39) {
                $('.modal-next').trigger('click');
            } else if (e.keyCode === 82) {
                $('.modal-refresh').trigger('click');
            } else if (e.keyCode === 32) {
                $('.modal-checkbox').trigger('click');
            }
        });

        $('.modal-checkbox').on('click', checkFileToggle);
    };

    var checkFileToggle = function () {
        var $modal = $('#preview-modal');
        var $checkbox = $modal.data('current').closest('tr').find('input');
        var checked = !$checkbox.prop('checked');
        $checkbox.trigger('click');
        $modal.find('.modal-checkbox').toggleClass('checked', checked).toggleClass('unchecked', !checked);

        if (checked) {
          $modal.find('.modal-checkbox i').removeClass('mdi-checkbox-blank-outline').addClass('mdi-check-box-outline')
        } else {
          $modal.find('.modal-checkbox i').removeClass('mdi-check-box-outline').addClass('mdi-checkbox-blank-outline')
        }

        return false;
    };

    var checkAllToggle = function () {
        $('#check-all').prop('checked', !$('.default-view-files table .check input:enabled:not(:checked)').length);
    };

    var initConfirmationCheckboxes = function () {
        $('#check-all').on('change', function () {
            $('.default-view-files table .check input:enabled').prop('checked', this.checked);
        });

        $('.default-view-files table .check input').click(function () {
            checkAllToggle();
        });

        checkAllToggle();
    };

    var initToggleActions = function () {
        $('#action-toggle').find(':input').change(function () {
            $(this).parent('label').toggleClass('active', this.checked);
            var $rows = $('.' + this.value, '.default-view-files table').toggleClass('action-hidden', !this.checked);
            if (this.checked) {
                $rows.not('.filter-hidden').show();
            } else {
                $rows.hide();
            }
            $rows.find('.check input').attr('disabled', !this.checked);
            checkAllToggle();
        });
    };

    var initFilterRows = function () {
        $('#filter-input').on('input', function () {
            var that = this,
            $rows = $('#files-body').find('tr');

            $rows.hide().toggleClass('filter-hidden', true).filter(function () {
                return $(this).text().toUpperCase().indexOf(that.value.toUpperCase()) > -1;
            }).toggleClass('filter-hidden', false).not('.action-hidden').show();

            $rows.find('input').each(function(){
                $(this).prop('disabled', $(this).is(':hidden'));
            });
        });
    };

    return {
        init: function () {
            initPreviewDiffLinks();
            initConfirmationCheckboxes();
            initToggleActions();
            initFilterRows();

            // hide Generate button if any input is changed
            $('#form-fields').find('input,select,textarea').change(function () {
                $('.default-view-results,.default-view-files').hide();
                $('.default-view button[name="generate"]').hide();
            });

            $('.module-form #generator-moduleclass').change(function () {
                var value = $(this).val().match(/(\w+)\\\w+$/);
                var $idInput = $('#generator-moduleid');
                if (value && value[1] && $idInput.val() === '') {
                    $idInput.val(value[1]);
                }
            });
        }
    };
})(jQuery);
