[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | Module developer tools
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Possibility to have multiple primary columns @cmoeke
- Appropriated input for email, phone, telephone, tel, mobile, fax, url and website attribute in field-config.php @StefanBrandenburger
- Abstract model template. @cmoeke
- Property generatedModelClass in Generator.php#55 to store currently generated model class name @StefanBrandenburger
- Added headline to possible primary columns @cmoeke
- Generate rules in FAFCMS Model Generator for date / time / datetime columns @cmoeke #1
- Generate rules in FAFCMS Model Generator for boolean columns @cmoeke #4 
- Option to use real classes and ignore the injector component configuration @cmoeke
- Option to pass columns which should be ignored at base model generation @cmoeke #5
- Option to remove relations of specified namespaces at base model generation @cmoeke #6
- Column index to index view @cmoeke
- New OptionProvider and OptionProviderTrait @cmoeke

### Changed
- Nearly completely rewritten fafcms model generator @cmoeke
- Implementation of AttributeOptionTrait @StefanBrandenburger
- Search model now generates with return types @StefanBrandenburger
- Changed rules to make it easier to replace them @cmoeke
- Changed abstract base model class name @cmoeke
- Improved multiple relation names @cmoeke
- Replaced self with static in option.php @StefanBrandenburger
- Function useClass now returns static if the class name equals the currently generated model class name @StefanBrandenburger
- Closures in AttributeOptionTrait implementation accepts now variable number of arguments, and they are passed to getOptions call in attribute-option.php @StefanBrandenburger
- Changed option trait to use static function @cmoeke
- Improved filter for email, phone and url field config to only apply when column is varchar or text @cmoeke
- Improved filter for email field config @cmoeke
- Retrieval of attributeOptions to getAttributeOptions @cmoeke
- Column sizes for fomantic @cmoeke
- Changed menu items @cmoeke

### Fixed
- Fixed model file path @cmoeke
- Bug of primary column linking @cmoeke
- Wrong column definition in index view interface @cmoeke
- Fixed bug when generating exist rule with own model class @cmoeke
- Filter custom fields in model generation @cmoeke #3
- Table name of contentmeta_topic gets generated wrong @cmoeke #2

### Removed
- Useless backslash in class usage @cmoeke 

## [0.1.0] - 2020-09-29
### Added
- CHANGELOG.md @cmoeke fafcms-core#39
- basic doc folder structure @cmoeke fafcms-core/37
- .gitlab-ci.yml for doc an build creation @cmoeke fafcms-core/38

### Changed
- composer.json to use correct dependencies @cmoeke
- PHP dependency to 7.4 @cmoeke
- Changed ci config to handle new build branch @cmoeke

### Fixed
- Broken README.md icons @cmoeke fafcms-core#46

[Unreleased]: https://gitlab.com/finally-a-fast/fafcms-module-developer-tools/-/tree/master
[0.1.0]: https://gitlab.com/finally-a-fast/fafcms-module-developer-tools/-/tree/v0.1.0-alpha
